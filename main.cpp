#include <iostream>
#include "Lifts.h"
#include "LiftsTest.cpp"


int main(int argc, char **argv)
{
	//запуск тестов
	if(argc > 1 && !strcmp(argv[1],"-t"))
	{
		testing::InitGoogleTest(&argc, argv);
    	return RUN_ALL_TESTS();
	}

	//время простоя лифта, через которое он вернется на 1ый этаж. '0' - опция отключена.
	const size_t come_back_time = 1;

	//Создаем менеджер. Передаем параметры лифтов.
	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	//Добавление событий только после запуска менеджера. Иначе они проигнорируются.
	lift_manager.start();
	lift_manager.add({ 0,  {1, Action::UP,   5 } });
	lift_manager.add({ 10, {3, Action::UP,   10} });
	lift_manager.add({ 15, {7, Action::DOWN, 1 } });

	//При остановке менеджера обрабатываются все события, и лифты возвращаются на 1ый этаж(если опция включена)
	lift_manager.stop();
	
	//Получаем список действий.
	//Этот список можно получить и до остановки менеджера, но он может быть неполный, т.к.
	//последние события могут быть не обработаны. Их обработка зависит от последующих событий!
	auto actions = lift_manager.getActions();
	std::cout << "Time	Lift	Floor	Action\n";
	std::cout << "------------------------------\n";
	for (const auto& action : actions)
		std::cout << action.first << "	" << action.second.lift_num << "	" << action.second.floor << "	" << LiftManager::getActionStr(action.second.action) << std::endl;
}