#include "Lifts.h"
#include <gtest/gtest.h>


bool operator== (const ActionData &lhs, const ActionData &rhs)
{
	return (lhs.action == rhs.action && lhs.floor == rhs.floor && lhs.lift_num == rhs.lift_num);
}

void test_actions(LiftManager& lift_manager, std::multimap<size_t, ActionData>& ref_actions)
{
	lift_manager.stop();
	auto actions = lift_manager.getActions();
    ASSERT_EQ(actions.size(), ref_actions.size());
	for(auto it = actions.begin(), ref_it = ref_actions.begin(); it != actions.end(); it++, ref_it++)
		ASSERT_EQ(*it, *ref_it);
}


TEST(LiftsTest, come_back_off_and_same_direction_off)
{
	const size_t come_back_time = 0;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	lift_manager.start();
	lift_manager.add({0,  {1, Action::UP,   5 }});
	lift_manager.add({10, {3, Action::UP,   10}});
	lift_manager.add({15, {7, Action::DOWN, 1 }});

    std::multimap<size_t, ActionData> ref_actions = {
		{0,  {1, 1,  Action::UP}},
		{8,  {1, 5,  Action::STOP}},
		{10, {1, 5,  Action::DOWN}},
		{14, {1, 3,  Action::STOP}},
		{14, {1, 3,  Action::UP}},
		{15, {2, 1,  Action::UP}},
		{27, {2, 7,  Action::STOP}},
		{27, {2, 7,  Action::DOWN}},
		{28, {1, 10, Action::STOP}},
		{39, {2, 1,  Action::STOP}}
	};

    test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_on_and_same_direction_off)
{
	const size_t come_back_time = 1;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});
	
	lift_manager.start();
	lift_manager.add({ 0,  {1, Action::UP,   5 }});
	lift_manager.add({ 10, {3, Action::UP,   10}});
	lift_manager.add({ 15, {7, Action::DOWN, 1 }});

    std::multimap<size_t, ActionData> ref_actions = {
		{0,  {1, 1,   Action::UP}},
		{8,  {1, 5,   Action::STOP}},
		{9,  {1, 5,   Action::DOWN}},
		{10, {2, 1,   Action::UP}},
		{14, {2, 3,   Action::STOP}},
		{14, {2, 3,   Action::UP}},
		{17, {1, 1,   Action::STOP}},
		{17, {1, 1,   Action::UP}},
		{28, {2, 10,  Action::STOP}},
		{29, {1, 7,   Action::STOP}},
		{29, {1, 7,   Action::DOWN}},
		{29, {2, 10,  Action::DOWN}},
		{41, {1, 1,   Action::STOP}},
		{47, {2, 1,   Action::STOP}}
	};

    test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_off_and_same_direction_on_1)
{
	const size_t come_back_time = 0;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	lift_manager.start();
	lift_manager.add({6, {1, Action::UP, 7}});
	lift_manager.add({8, {3, Action::UP, 5}});

    std::multimap<size_t, ActionData> ref_actions = {
		{6,  {1, 1,   Action::UP}},
		{10, {1, 3,   Action::STOP}},
		{10, {1, 3,   Action::UP}},
		{14, {1, 5,   Action::STOP}},
		{14, {1, 5,   Action::UP}},
		{18, {1, 7,   Action::STOP}},
	};

	test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_off_and_same_direction_on_2)
{
	const size_t come_back_time = 0;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	lift_manager.start();
	lift_manager.add({6, {1, Action::UP, 7}});
	lift_manager.add({8, {3, Action::UP, 9}});

    std::multimap<size_t, ActionData> ref_actions = {
		{6,  {1, 1,   Action::UP}},
		{10, {1, 3,   Action::STOP}},
		{10, {1, 3,   Action::UP}},
		{18, {1, 7,   Action::STOP}},
		{18, {1, 7,   Action::UP}},
		{22, {1, 9,   Action::STOP}},
	};

	test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_off_and_same_direction_on_3)
{
	const size_t come_back_time = 0;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	lift_manager.start();
	lift_manager.add({6, {1, Action::UP, 7}});
	lift_manager.add({8, {3, Action::UP, 7}});

    std::multimap<size_t, ActionData> ref_actions = {
		{6,  {1, 1,   Action::UP}},
		{10, {1, 3,   Action::STOP}},
		{10, {1, 3,   Action::UP}},
		{18, {1, 7,   Action::STOP}}
	};

	test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_off_and_same_direction_on_4)
{
	const size_t come_back_time = 0;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	lift_manager.start();
	lift_manager.add({4, {1, Action::UP, 3}});
	lift_manager.add({6, {1, Action::UP, 7}});
	lift_manager.add({8, {3, Action::UP, 7}});

    std::multimap<size_t, ActionData> ref_actions = {
		{4,  {1, 1,   Action::UP}},
		{6,  {2, 1,   Action::UP}},
		{8,  {1, 3,   Action::STOP}},
		{8,  {1, 3,   Action::UP}},
		{16, {1, 7,   Action::STOP}},
		{18, {2, 7,   Action::STOP}}
	};

	test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_off_and_same_direction_on_5)
{
	const size_t come_back_time = 0;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});

	lift_manager.start();
	lift_manager.add({4, {1, Action::UP, 8}});
	lift_manager.add({6, {3, Action::UP, 5}});
	lift_manager.add({8, {6, Action::UP, 9}});

    std::multimap<size_t, ActionData> ref_actions = {
		{4,  {1, 1,   Action::UP}},
		{8,  {1, 3,   Action::STOP}},
		{8,  {1, 3,   Action::UP}},
		{12, {1, 5,   Action::STOP}},
		{12, {1, 5,   Action::UP}},
		{14, {1, 6,   Action::STOP}},
		{14, {1, 6,   Action::UP}},
		{18, {1, 8,   Action::STOP}},
		{18, {1, 8,   Action::UP}},
		{20, {1, 9,   Action::STOP}}
	};

	test_actions(lift_manager, ref_actions);
}


TEST(LiftsTest, come_back_on_and_same_direction_on)
{
	const size_t come_back_time = 1;

	LiftManager lift_manager({
		{2, come_back_time},
		{2, come_back_time},
		{3, come_back_time}
	});
	
	lift_manager.start();
	lift_manager.add({4,  {1, Action::UP, 3}});
	lift_manager.add({8,  {1, Action::UP, 7}});
	lift_manager.add({10, {3, Action::UP, 7}});

    std::multimap<size_t, ActionData> ref_actions = {
		{4,  {1, 1,   Action::UP}},
		{8,  {1, 3,   Action::STOP}},
		{8,  {2, 1,   Action::UP}},
		{9,  {1, 3,	  Action::DOWN}},
		{12, {2, 3,   Action::STOP}},
		{12, {2, 3,   Action::UP}},
		{13, {1, 1,	  Action::STOP}},
		{20, {2, 7,   Action::STOP}},
		{21, {2, 7,   Action::DOWN}},
		{33, {2, 1,   Action::STOP}}
	};

	test_actions(lift_manager, ref_actions);
}
