#pragma once
#include <string>
#include <vector>
#include <map>
#include <thread>
#include <mutex>
#include <atomic>

//включение(1)/отключение(0) опции подбора пассажиров в сторону движения
#define ON_WAY_OPTION 1


enum class Action
{
	STOP,
	UP,
	DOWN
};


struct ActionData
{
	size_t lift_num;
	size_t floor;
	Action action;
};


struct EventData
{
	size_t start_floor;
	Action action;
	size_t end_floor;
};


struct LiftData
{
	size_t speed;
	size_t come_back_time;
};


class Lift
{
public:
	Lift(std::mutex& log_mutex, std::multimap<size_t, ActionData>& log, const size_t num, const size_t speed, const size_t come_back_time = 0);
	Lift(const Lift& lift);
	void start();
	void stop();
	static void process(Lift& lift);
	void set_event(std::pair<const size_t, EventData> event);
	size_t get_time(const std::pair<const size_t, EventData>& event);
	void come_back(const size_t event_time = SIZE_MAX);

private:
	void change_state(const size_t next_floor);
	inline size_t get_time(const size_t start_floor, const size_t end_floor);
	bool on_way(const std::pair<const size_t, EventData>& event);
	std::pair<const size_t, EventData>& get_last_event();
	void reset() { busy_time = 0; cur_floor = 1; end_time = 0; end_floor = 1; events_count = 0;  log.clear(); events.clear(); }

	const size_t num;
	const size_t speed;
	const size_t come_back_time;
	size_t busy_time;
	size_t cur_floor;
	size_t end_time;
	size_t end_floor;
	std::map<const size_t, EventData> events;
	std::atomic<size_t> events_count;
	std::atomic<bool> started;
	std::mutex events_mutex;
	std::thread thread;
	std::mutex& log_mutex;
	std::multimap<size_t, ActionData>& log;
};


class LiftManager
{
public:
	LiftManager(const std::vector<LiftData>& lift_data);
	~LiftManager() { stop(); }
	static std::string getActionStr(const Action action);
	std::multimap<size_t, ActionData> getActions();
	void add(const std::pair<const size_t, const EventData>& event);
	void start();
	void stop();

private:
	size_t get_best_lift_idx(const std::pair<const size_t, EventData>& event);

	size_t last_event_time;
	std::vector<Lift> lifts;
	std::mutex log_mutex;
	std::multimap<size_t, ActionData> log;
	bool started;
};
