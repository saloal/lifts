#include "Lifts.h"


Lift::Lift(std::mutex& log_mutex, std::multimap<size_t, ActionData>& log, const size_t num, const size_t speed, const size_t come_back_time/*=0*/) :
	num(num),
	speed(speed),
	come_back_time(come_back_time),
	busy_time(0),
	cur_floor(1),
	end_time(0),
	end_floor(1),
	events_count(0),
	started(false),
	log_mutex(log_mutex),
	log(log)
{}


Lift::Lift(const Lift& lift) : 
	num(lift.num),
	speed(lift.speed),
	busy_time(lift.busy_time),
	cur_floor(lift.cur_floor),
	end_time(lift.end_time),
	end_floor(lift.end_floor),
	come_back_time(lift.come_back_time),
	events_count(lift.events_count.load()),
	started(lift.started.load()),
	log_mutex(lift.log_mutex),
	log(lift.log)
{}


inline size_t Lift::get_time(const size_t start_floor, const size_t end_floor)
{
	return abs((int)(start_floor - end_floor)) * speed;
}


bool Lift::on_way(const std::pair<const size_t, EventData>& event)
{
	if (!ON_WAY_OPTION || events.empty())
		return false;

	bool res = false;
	const auto& last_event = get_last_event();
	if (event.second.action != Action::STOP && event.second.action == last_event.second.action)
		res = (event.second.action == Action::UP) ? (event.second.start_floor >= last_event.second.start_floor) :
															      (event.second.start_floor <= last_event.second.start_floor) ;
	if (!res)
		return false;

	const size_t time_diff = event.first - last_event.first;
	if (get_time(last_event.second.start_floor, event.second.start_floor) >= time_diff)
		return true;

	return false;
}


std::pair<const size_t, EventData>& Lift::get_last_event()
{
	std::map<const size_t, EventData>::reverse_iterator last_event = events.rbegin();
	while(last_event->second.action == Action::STOP)
		last_event++;
	return *last_event;
}


size_t Lift::get_time(const std::pair<const size_t, EventData>& event)
{
	const std::lock_guard<std::mutex> events_lock(events_mutex);
	size_t time = SIZE_MAX;

	//Если время события меньше предыдущего, не рассматриваем его
	if(!events.empty())
	{
		const auto& last_event = get_last_event();
		if(event.first < last_event.first)
			return time;
	}

	//Если лифт может подобрать пассажиров по пути, то считаем время.
	//Возможно другой лифт окажется ближе, и он выполнит задачу быстрее.	
	if (on_way(event))
	{
		const auto& last_event = get_last_event();
		const size_t time_diff = event.first - last_event.first;
		time = get_time(last_event.second.start_floor, event.second.start_floor) - time_diff;
	}
	else
	{
		//если лифт еще занят, считаем задержку
		time = (end_time > event.first) ? (end_time - event.first) : 0;
		time += get_time(end_floor, event.second.start_floor);
	}
	time += get_time(event.second.start_floor, event.second.end_floor);
	return time;
}


void Lift::set_event(std::pair<const size_t, EventData> event)
{
	const std::lock_guard<std::mutex> events_lock(events_mutex);

	if (on_way(event))
	{
		//Если лифт может подобрать пассажиров по пути, то необходимо выделить основное событие с максимальным диапазоном этажей и остановками в нем
		auto& last_event = get_last_event();

		if (last_event.second.start_floor != event.second.start_floor)
		{
			const size_t new_event_time = end_time - get_time(end_floor, event.second.start_floor);
			events.insert({ new_event_time, {event.second.start_floor, Action::STOP, event.second.start_floor}});
		}

		if((event.second.action == Action::UP) ? (last_event.second.end_floor < event.second.end_floor) : (last_event.second.end_floor > event.second.end_floor))
		{
			events.insert({ end_time, {end_floor, Action::STOP, end_floor}});
			last_event.second.end_floor = event.second.end_floor;

			//Эти переменные нужны для распределения событий между лифтами,
			//чтобы знать конечное положение лифта после всех событий.
			end_time += get_time(event.second.end_floor, end_floor);
			end_floor = event.second.end_floor;			
		}
		else if((event.second.action == Action::UP) ? (last_event.second.end_floor > event.second.end_floor) : (last_event.second.end_floor < event.second.end_floor))
		{
			const size_t new_event_time = end_time - get_time(end_floor, event.second.end_floor);
			events.insert({ new_event_time, {event.second.end_floor, Action::STOP, event.second.end_floor}});
		}
	}
	else
	{
		end_time = std::max(end_time, event.first);
		end_time += get_time(end_floor, event.second.start_floor);
		end_time += get_time(event.second.end_floor, event.second.start_floor);
		end_floor = event.second.end_floor;

		events.insert(event);
		events_count++;
	}
}


void Lift::change_state(const size_t next_floor)
{
	//если этажи свопадают, ничего не делаем
	if (cur_floor == next_floor)
		return;

	//определяем направление лифта и записываем событие
	const Action action = cur_floor > next_floor ? Action::DOWN : Action::UP;
	log.insert({ busy_time, {num, cur_floor, action } });

	//обновлеям данные лифта и записываем событие остановки на заданном этаже
	busy_time += get_time(cur_floor, next_floor);
	cur_floor = next_floor;
	log.insert({ busy_time, { num, cur_floor, Action::STOP } });
}


void Lift::come_back(const size_t event_time/*=SIZE_MAX*/)
{
	if (!come_back_time || end_floor == 1 || event_time <= end_time)
		return;

	if ((event_time - end_time) >= come_back_time)
		set_event({ end_time + come_back_time, { end_floor, Action::DOWN, 1} });
}


void Lift::process(Lift& lift)
{
	while (lift.started || lift.events_count)
	{
		//Всегда держим последнее событие, чтобы можно было забрать пассажиров по пути.
		//Обрабатываем это событие только после остановки лифта.
		if (!lift.events_count || (ON_WAY_OPTION && lift.started && lift.events_count == 1))
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(5));
			continue;
		}

		//берем первое событие на обработку
		const std::lock_guard<std::mutex> events_lock(lift.events_mutex);
		std::map<const size_t, EventData>::iterator event = lift.events.begin();

		//Считаем время, когда лифт начнет выполнять событие.
		//Начиная с этого времени поэтажно меняем состояние лифта.
		lift.busy_time = std::max(lift.busy_time, event->first);
		
		const std::lock_guard<std::mutex> log_lock(lift.log_mutex);
		lift.change_state(event->second.start_floor);
		lift.change_state(event->second.end_floor);
		
		//считаем, что событие выполнено и перемещаем его на удаление
		std::vector<size_t> remove_event_times;
		remove_event_times.push_back(event->first);
		
		//Если сразу после выполненного события есть события остановки, то это случаи подбора пассажиров в сторону движения лифта.
		//Обрабатываем их и перемещаем на удаление.
		const Action action = event->second.action;
		for(event++; event != lift.events.end() && event->second.action == Action::STOP; event++)
		{		
			lift.log.insert({event->first, {lift.num, event->second.start_floor, Action::STOP}});
			lift.log.insert({event->first, {lift.num, event->second.start_floor, action}});
			remove_event_times.push_back(event->first);
		}

		//удаляем все выполненные события
		for(const auto& remove_event_time : remove_event_times)
			lift.events.erase(remove_event_time);

		lift.events_count--;
	}
}


void Lift::start()
{
	if (!started)
	{
		reset();
		started = true;
		thread = std::thread(process, std::ref(*this));
	}
}


void Lift::stop()
{
	if (started)
	{
		come_back();
		started = false;
		if (thread.joinable())
			thread.join();
	}
}


//----------------------------------------------------------------
//----------------------------------------------------------------
LiftManager::LiftManager(const std::vector<LiftData>& lift_data) : last_event_time(0), started(false)
{
	for (const auto& lift : lift_data)
		lifts.push_back(Lift(log_mutex, log, lifts.size() + 1, lift.speed, lift.come_back_time));
}


void LiftManager::start()
{
	if (started || lifts.empty())
		return;

	for (auto& lift : lifts)
		lift.start();

	started = true;
}


void LiftManager::stop()
{
	if (!started || lifts.empty())
		return;

	for (auto& lift : lifts)
		lift.stop();

	started = false;
}


std::string LiftManager::getActionStr(const Action action)
{
	switch (action)
	{
	case Action::STOP:
		return "STOP";
	case Action::UP:
		return "UP";
	case Action::DOWN:
		return "DOWN";
	default:
		return "";
	}
}


void LiftManager::add(const std::pair<const size_t, const EventData>& event)
{
	if (!started)
		return;

	//исключаем невозможные события
	if (event.second.action == Action::STOP || event.second.start_floor == event.second.end_floor)
		return;
	if (event.second.action == Action::UP ? event.second.start_floor > event.second.end_floor : event.second.start_floor < event.second.end_floor)
		return;

	//события должны идти последовательно
	if (event.first < last_event_time)
		return;

	last_event_time = event.first;

	//ищем лифт, который быстрее всех выполнит событие, и передаем лифту это событие
	const size_t lift_idx = get_best_lift_idx(event);
	if (lift_idx < lifts.size())
		lifts[lift_idx].set_event(event);
}


std::multimap<size_t, ActionData> LiftManager::getActions()
{
	const std::lock_guard<std::mutex>log_lock(log_mutex);
	return log;
}


size_t LiftManager::get_best_lift_idx(const std::pair<const size_t, EventData>& event)
{
	size_t min_time = SIZE_MAX, lift_idx = lifts.size();

	for (size_t i = 0; i < lifts.size(); i++)
	{
		//возврат лифта на 1 этаж, если нужно
		lifts[i].come_back(event.first);

		//Время считается исходя из текущего положения лифта, неважно занят он или нет.
		//Занятый лифт может приехать быстрее, чем свободный.
		const size_t time = lifts[i].get_time(event);

		if (time < min_time)
		{
			lift_idx = i;
			min_time = time;
		}
	}

	return lift_idx;
}
